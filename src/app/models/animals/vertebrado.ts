import {Animal} from './animal'

export class Vertebrado extends Animal{

    protected tipo:string

    constructor(name:string, tipo:string){
        super(name, true)
        this.tipo = tipo
    }

    describir(){
        let base = super.describir()
        return base + " " + this.tipo
    }

}


