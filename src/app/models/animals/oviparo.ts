import {Vertebrado} from './vertebrado'

export class Oviparo extends Vertebrado{

    constructor(name:string){
        super(name,"Vertebrado")
    }

    setName(name:string){
        this.name = name
    }
}
