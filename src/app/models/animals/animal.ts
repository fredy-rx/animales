import {IAnimal} from '../../interfaces/animal'

export class Animal implements IAnimal{
    
    name:string
    protected esVertebrado:boolean

    constructor(name:string, esVertebrado:boolean=true){
        this.name = name
        this.esVertebrado = esVertebrado
    }

    setName(name:string){
        this.name = name
    }

    presentar(){
        console.log("el animal "+this.name)
    }


    describir(): string {
        return this.esVertebrado?"Vertebrado":"Invertebrado";
    }

}
