import {Vertebrado} from './vertebrado'

export class Mamifero extends Vertebrado {
    
    constructor(name:string){
        super(name, "Mamifero")
    }
}
