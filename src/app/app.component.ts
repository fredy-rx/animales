import { Component } from '@angular/core';

import { Animal } from './models/animals/animal'
import { Vertebrado } from './models/animals/vertebrado'
import { Mamifero } from './models/animals/mamifero'
import { Oviparo } from './models/animals/oviparo'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  animalesList: Animal[] = []

  constructor(){
    this.crearAnimales()
  }

  crearAnimales(){
    this.animalesList.push(new Animal("Caballo"))
    this.animalesList.push(new Vertebrado("Toro","Mamifero"))
    this.animalesList.push(new Mamifero("Caballo"))
    this.animalesList.push(new Oviparo("Aguila"))
  }
}
