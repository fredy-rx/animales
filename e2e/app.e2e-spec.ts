import { ClaseMiercoles0908Page } from './app.po';

describe('clase-miercoles0908 App', () => {
  let page: ClaseMiercoles0908Page;

  beforeEach(() => {
    page = new ClaseMiercoles0908Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
